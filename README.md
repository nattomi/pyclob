## PyClob: Processing CLOBS in Python

PyClob is a simple code for processing Character Large Objects (CLOBs). At present it's only implemented functionality is the possibility to count occurences of a multiline pattern in a CLOB stored in an ASCII file.

### Quickstart

```
>>> import clob
>>> p = clob.Pattern(("x x",
...                   " y "))
>>> c = clob.Clob(("x x x x x",
...                " y y y y y"))
>>> c.patternCounter(p)
4
``` 

If the clob is large and stored in a file then process it as follows

```
>>> c = clob.Clob(("pathToLargeASCIIFile.txt")
>>> c.patternCounter(p)
```

### Building

```
python setup.py build_ext --inplace
```

Optionally, run the test suites:

```
python test_clob.py -v
python clob.py -v
```

For a demo app, run:

```
python demo.py data/bug.txt data/landscape.txt
```

### Details

Consider the following clob:

```
c = clob.Clob((" I I    I I        I I ",
               " +++x   +++x @@@@@ ++++",
	           " I I  I I I  @I@I@ I I ",
	           "      +++x   @+++x     ",
	           "      I I    @I@I@     ",
               "             @@@@@     "))
```

and pattern:

```
p = clob.Pattern(("I I ",
                  "+++x",
				  "I I "))
```

Then, there are 5 possible occurrences of the pattern in the clob:

* The one at position (1, 2), is a hit without a doubt.
* We consider the ones at positions (1, 9) and (3, 7) hits as well, even though they overlap. We count them as two separate hits.
* The one at position (3, 15) is admittedly hidden very well, but it's there, since we don't take into account whitespaces in the pattern. It's a hit too.
* The one at (1, 20) is a near hit, but there is one character difference.

To sum it up, `c.patternCounter(p)` will give `4` as a result.

### Known limitations

The module is not ready for handling characters outside of the ASCII range and it's not tested with such inputs.

