#include <Python.h>
#include "src/clob.h"

/* Docstrings */
static char module_docstring[] = 
  "This module provides an interface for looking up a \
multi-line pattern in a character large object using C";
static char patternCounter_docstring[] =
  "Count occurrences of a multiline pattern in a \
character large object";

/* Available functions */
static PyObject *clob_patternCounter(PyObject *self, PyObject *args);

/* Module specification */
static PyMethodDef module_methods[] = {
  {"patternCounter", clob_patternCounter, METH_VARARGS, patternCounter_docstring},
  {NULL, NULL, 0, NULL}
};

/* Initialize the module */
PyMODINIT_FUNC init_clob(void) {
  PyObject *m = Py_InitModule3("_clob", module_methods, module_docstring);
  if(m == NULL)
    return;
}

/* The interface */
static PyObject *clob_patternCounter(PyObject *self, PyObject *args) {

  PyObject *tuple_pattern;
  PyObject *tuple_clob;

  /* Parse the input tuple */
  if(!PyArg_ParseTuple(args, "OO", &tuple_pattern, &tuple_clob))
    return NULL;
  
  //int b = PyTuple_Check(tuple_obj);

  int numberOfLines = PyTuple_Size(tuple_pattern);

  char* pattern[numberOfLines];
  char* clob[numberOfLines];
  
  int i = 0;
  for(; i < numberOfLines; ++i) {
    PyObject *pattern_line = PyTuple_GetItem(tuple_pattern, i);
    pattern[i] = PyString_AsString(pattern_line);
    PyObject *clob_line = PyTuple_GetItem(tuple_clob, i);
    clob[i] = PyString_AsString(clob_line);
  }
  
  /* Call the external C function */
  int counter = patternCounter(numberOfLines, pattern, clob);
  
  return Py_BuildValue("i", counter);
}
