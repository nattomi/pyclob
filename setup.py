# -*- coding: utf-8 -*-

from distutils.core import setup, Extension
import os

setup(
    ext_modules=[Extension("_clob", ["_clob.c", os.path.join("src", "clob.c")])],
)
