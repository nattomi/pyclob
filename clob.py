# -*- coding: utf-8 -*-
"""
Counting multiline patterns in character large objects (CLOBs).

Quickstart
----------
Defining a 'Pattern' and a 'Clob' object and counting the
number of occurrences of 'Pattern' in 'Clob'. 

>>> import clob
>>> p = clob.Pattern(("x x",
...                   " y "))
>>> c = clob.Clob(("x x x x x",
...                " y y y y y"))
>>> c.patternCounter(p)
4

The 'Pattern' and 'Clob' objects can either be constructed
from a list or a tuple as shown above or from a string
describing a file path. For example:

>>> import os
>>> p = clob.Pattern(os.path.join("data", "bug.txt"))
>>> c = clob.Clob(os.path.join("data", "landscape.txt"))
>>> c.patternCounter(p)
3

"""

# we rely on the core module, which is written in C
import _clob

class Pattern:
    """
    Simple class for defining multiline patterns

    >>> import clob
    >>> p = clob.Pattern(("x x",
    ...                   " y "))
    >>> print p
    x x
     y
    >>> len(p)
    2

    Trailing whitespaces are removed:

    >>> pFlip = clob.Pattern((" y      ",
    ...                       "x x     "))
    >>> pFlip.data
    (' y', 'x x')

    Patterns can be added too
    >>> print p + pFlip
    x x
     y
     y
    x x

    Patterns can be read from a file too

    >>> import os
    >>> print clob.Pattern(os.path.join("data", "bug2.txt"))
    I I
    +++x
    I I

    """

    def __init__(self, stream):
        if isinstance(stream, str): # a file path is supplied
            with open(stream, 'r') as f:
                self.parseStream(f)
        else: # we try to interpret the supplied argument as a tuple
            self.parseStream(tuple(stream))

    # pretty-printing a pattern
    def __str__(self):
        return '\n'.join(self.data)

    # getting length of a pettern
    def __len__(self):
        return len(self.data)
    
    # adding two patterns
    def __add__(self, p): 
        return Pattern(self.data + p.data)  
        
    # this is only needed because for the sake of
    # easier testing we accept both an iterable 
    # and a file path in the constructor
    def parseStream(self, iterable):
        data = ()
        for line in iterable:
            if not isinstance(line, str):
                raise TypeError('An ASCII string expected.')
            # it's not guaranteed that a line
            # supplied as an item in a tuple
            # or a list only contains newline
            # at the end, therefore we are
            # sanitizing the line here
            line = line.replace('\n', '')
            # we have to remove trailing whitespaces
            # as the underlying C module expects
            # it in this way
            data += (line.rstrip(), )
        self.data = data
        
    def isWhitespaceOnly(self):
        """
        Gives True if the pattern is degenerate, i.e.
        each of its lines are essentially whitespace only

        >>> import clob
        >>> clob.Pattern((" ", "x")).isWhitespaceOnly()
        False
        >>> clob.Pattern((" ", " ")).isWhitespaceOnly()
        True
        """
        return all(len(i) == 0 for i in self.data)

class Clob:
    """
    Simple class for defining a character large objects.
    
    >>> import clob
    >>> c = clob.Clob(("xxxyyyxxxy",
    ...                "yyyxxxyyyx"))

    Run the 'patternCounter' method to search for occurrences
    of a pattern.

    >>> c.patternCounter(clob.Pattern(("xy", 
    ...                                "yx")))
    2

    A more realistic use case is when the clob is
    actually large and stored in a file.

    >>> import os
    >>> c = clob.Clob(os.path.join("data", "landscape_overlapping.txt"))
    
    """

    def __init__(self, stream):
        if isinstance(stream, str): # a file path is supplied
            self.data = stream
        else: # we try to interpret the supplied argument as a tuple
            self.data = tuple(stream)
 
    def patternCounter(self, pattern):
        """ Counting the number of occurrences of a 'Pattern' in a 'Clob'. """
        if isinstance(self.data, str): # clob is in a file
            with open(self.data, 'r') as f:
                return Clob.patternCounterInStream(pattern, f)
        else: # clob is a tuple or a list
            return Clob.patternCounterInStream(pattern, self.data)
        
    # it has to be in a separate method because
    # for the sake of easier testing we accept 
    # both an iterable and a file path in the constructor
    @staticmethod
    def patternCounterInStream(pattern, stream):
        numberOfLines = len(pattern)
        # We just can't do anything with essentially empty patterns
        if pattern.isWhitespaceOnly() or not numberOfLines: 
            raise ValueError("Pattern is essentially empty.")
        clob = []
        counter = 0
        for line in stream:
            # if the source is a tuple or a string
            # it's not guaranteed that the caller supplied
            # valid items
            if not isinstance(line, str):
                raise TypeError('An ASCII string expected.')
            # updating the subset of the clob
            # which is subject to search
            if len(clob) == numberOfLines:
                del clob[0]
            clob += [line.replace('\n', '')]
            # We don't do anything until we have enough line
            # for the search to make sense
            if len(clob) == numberOfLines:
                # the actual counting using the 
                # core module written in C
                counter += _clob.patternCounter(pattern.data, tuple(clob))
        return counter


if __name__ == "__main__":
    import doctest
    doctest.testmod()
