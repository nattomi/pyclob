# -*- coding: utf-8 -*-

import sys
import clob

def main():
    if len(sys.argv) < 3:
        print "Usage: python demo.py [pathToPatternFile] [pathToClobFile]"
        sys.exit(0)

    p = clob.Pattern(sys.argv[1])
    c = clob.Clob(sys.argv[2])
    print c.patternCounter(p)

if __name__ == "__main__":
    main()
