#include "clob.h"

/* Searches for occurrences of a multiline pattern in a clob */
int patternCounter(int numberOfLines, char** pattern, char** clob) {  
  // Both 'pattern' and 'clob' are expected to represent 
  // 'numberOfLines' lines. If 'clob' represents more,
  // then only the first 'numberOFLines' are considered.

  // we start pattern matching on the left and gradually
  // shift our pattern's position to the right
  int offset = 0; // current position in clob
  int counter = 0; // number of occurrences so far

 search: while(1) {
    int i = 0;
    // at a certain position, we do pattern matching
    // line by line
    for(; i < numberOfLines; ++i) {
      // getting a pointer to the i-th line of the pattern
      char *p = pattern[i]; 
      // getting a pointer to the i-th line of the clob
      // at the current position
      char *q = clob[i] + offset;
      while(*p != '\0') {
	// we reached end of clob, search is over
	if(*q == '\0') {
	  return counter;
	}
	// whitespaces(=ASCII 32) are skipped
	if(*p == 32) {
	  // jump to next character in both pattern
	  // and clob
	  p++;
	  q++;
	  continue;
	}
	// as soon as we find a mismatch
	// we shift our pattern to the right immediately
	if(*p++ != *q++) {
	  offset++;
	  goto search;
	}
      }
    }
    // if everything went well, there is a match
    offset++;
    counter++;
  }
  
}
