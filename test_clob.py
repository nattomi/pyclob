# -*- coding: utf-8 -*-

import unittest
import os
import clob

class PatternClassTests(unittest.TestCase):
    def test_bad_tuple(self):
        pattern = clob.Pattern(("p@tt3rn",))
        self.assertEqual(pattern.data, ("p@tt3rn",))
        with self.assertRaises(IOError):
            clob.Pattern(("p@tt3rn"))
    
    def test_tuple_containing_wrong_type(self):
        with self.assertRaises(TypeError):
            clob.Pattern(("p@tt3rn", 42))

    def test_not_convertable_to_tuple(self):
        with self.assertRaises(TypeError):
            clob.Pattern(42)

    def test_newline_elimination(self):
        pattern = clob.Pattern(("l\nine1\n", "lin\ne2"))
        self.assertEqual(pattern.data, ("line1", "line2"))
                
    def test_whitespace_elimination(self):
        pattern = clob.Pattern(("|  |   ", "  |  |  "))
        self.assertEqual(pattern.data, ("|  |", "  |  |"))

    def test_read_from_file(self):
        pattern = clob.Pattern(os.path.join("data", "bug.txt"))
        self.assertEqual(pattern.data, ("| |", 
                                        "###O",
                                        "| |"))
        pattern = clob.Pattern(os.path.join("data", "blank.txt"))
        self.assertEqual(pattern.data, ())
        
    def test_whitespace_only_pattern(self):
        pattern = clob.Pattern(("  ", "   "))
        self.assertEqual(pattern.data, ("", ""))
        self.assertEqual(pattern.isWhitespaceOnly(), True)
        pattern = clob.Pattern(("  ", "   |"))
        self.assertEqual(pattern.isWhitespaceOnly(), False)
        
    def test_len_method(self):
        self.assertEqual(len(clob.Pattern(())), 0)
        self.assertEqual(len(clob.Pattern(("line1", ))), 1)
        self.assertEqual(len(clob.Pattern(("line1", "line2"))), 2)

    def test_add_method(self):
        pattern1 = clob.Pattern(("a", "b"))
        pattern2 = clob.Pattern(("cd", "ef"))
        p = pattern1 + pattern2
        self.assertEqual(pattern1.data + pattern2.data, p.data)
        

class ClobClassTests(unittest.TestCase):
    def test_not_convertable_to_tuple(self):
        with self.assertRaises(TypeError):
            clob.Clob(4)

class PatternCounterTests(unittest.TestCase):
    def test_bad_tuple(self):
        c = clob.Clob(("Cl0b",))
        pattern = clob.Pattern(("p",))
        self.assertEqual(c.patternCounter(pattern), 0)
        with self.assertRaises(IOError):
            c = clob.Clob(("Cl0b"))
            c.patternCounter(pattern)
            
    def test_empty_pattern(self):
        c = clob.Clob(("Cl0b",))
        pattern = clob.Pattern(())
        with self.assertRaises(ValueError):
            c.patternCounter(pattern)
    
    def test_whitespace_only_pattern(self):
        c = clob.Clob(("line1", "line2"))
        pattern = clob.Pattern(("   ","  "))
        with self.assertRaises(ValueError):
            c.patternCounter(pattern)

    def test_tuple_containing_wrong_type(self):
        c = clob.Clob(("Cl0b", 42))
        pattern = clob.Pattern(("p@tt3rn", ))
        with self.assertRaises(TypeError):
            c.patternCounter(pattern)

    def test_newline_elimination(self):
        c = clob.Clob(("l\nine\n1\n", "l\nine2\n"))
        pattern = clob.Pattern(("li", "li"))
        self.assertEqual(c.patternCounter(pattern), 1)
        pattern = clob.Pattern(("e1", "e2"))
        self.assertEqual(c.patternCounter(pattern), 1)
                
    def test_read_from_file(self):
        c = clob.Clob(os.path.join("data", "landscape.txt"))
        pattern = clob.Pattern(("     | |", 
                                "     ###O",
                                "     | |"))
        self.assertEqual(c.patternCounter(pattern), 2)
        pattern = clob.Pattern(os.path.join("data", "bug.txt"))
        self.assertEqual(c.patternCounter(pattern), 3)

    def test_blank_line_in_pattern(self):
        pattern = clob.Pattern(("| ", "  ", " |"))
        c = clob.Clob((" | ", "@&@@@&", "  |    "))
        self.assertEqual(c.patternCounter(pattern), 1)

    def test_blank_line_at_end_of_pattern(self):
        pattern = clob.Pattern(("| ", " |", "  "))
        c = clob.Clob((" | ", "@&|@@&", "   |   "))
        self.assertEqual(c.patternCounter(pattern), 1)
        c = clob.Clob((" | ", "@&|@@&", "   |   ", "&&&&"))
        self.assertEqual(c.patternCounter(pattern), 2)

    def test_pattern_has_more_line_than_clob(self):
        pattern = clob.Pattern(("| ", " |", "  "))
        c = clob.Clob((" | ", "@&|@@&"))
        self.assertEqual(c.patternCounter(pattern), 0)

    def test_clob_is_blank(self):
        pattern = clob.Pattern(("|", "|"))
        c = clob.Clob(())
        self.assertEqual(c.patternCounter(pattern), 0)
        c = clob.Clob(os.path.join("data", "blank.txt"))
        self.assertEqual(c.patternCounter(pattern), 0)
        pattern = clob.Pattern(())
        with self.assertRaises(ValueError):
            c.patternCounter(pattern)

    def test_pattern_in_noise(self):
        # whitespaces are ignored when
        # matching characters
        pattern = clob.Pattern(("I I",
                                "+++x",
                                "I I"))
        c = clob.Clob(("aIaIa",
                       "a+++x",
                       "aI Ia"))
        self.assertEqual(c.patternCounter(pattern), 1)

        
    def test_overlapping_patterns(self):
        # 'Overlapping' patterns are considered 
        # separate occurrances
        pattern = clob.Pattern(("I I",
                                "+++x",
                                "I I"))
        c = clob.Clob(("I I   ",
                       "+++x  ",
                       "I I I ",
                       "  +++x",
                       "  I I "))
        self.assertEqual(c.patternCounter(pattern), 2)
        # same as above, but loading from files
        pattern = clob.Pattern(os.path.join("data", "bug2.txt"))
        c = clob.Clob(os.path.join("data", "landscape_overlapping.txt"))
        self.assertEqual(c.patternCounter(pattern), 2)

if __name__ == '__main__':
    unittest.main()
